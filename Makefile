

FILES := $(shell find . -maxdepth 1 -name "*.hs"; find . -path "./[A-Z]*.hs*" -name "*.hs")
USED  := $(shell (find . -maxdepth 1 -name "*.hs" -exec grep "^import" {} \; ; find . -path "./[A-Z]*.hs*" -name "*.hs" -exec grep "^import" {} \;) | grep -v "\-\-" | sed -r 's,(^| )[[:lower:]]+,,g' | awk '{ print $$1 }' | sort | uniq | sed 's,\.,/,g;s,$$,.hs,')
FOUND := $(shell echo .$(patsubst %,:%,$(foreach hs,$(USED),$(shell echo ../*/$(hs) | sed -r 's,^.*\*.*$$,,;s,^(\.\./[[:alnum:]\-]+)/.*$$,\1,'))) | sed 's, ,,g')

.PHONY: build clean echo

build: $(patsubst %.hs, %.build, $(FILES))

$(patsubst %.hs, %,       $(FILES)):
$(patsubst %.hs, %.o,     $(FILES)):
$(patsubst %.hs, %.run,   $(FILES)):
$(patsubst %.hs, %.ghci,  $(FILES)):
$(patsubst %.hs, %.check, $(FILES)):
$(patsubst %.hs, %.build, $(FILES)):
$(patsubst %.hs, %.clean, $(FILES)):

%:      %.hs
	@ghc --make -i"$(FOUND)" $<
	@(test -f $@ || (echo '#!/bin/sh\nexec ghci -i"$(FOUND)" $< "$$@"' > $@ && chmod +x $@))

%.o:    %.hs
	@ghc --make -i"$(FOUND)" $<

%.run:  %.hs
	@runhaskell -i"$(FOUND)" $<

%.ghci: %.hs
	@ghci       -i"$(FOUND)" $<

%.check: %.hs
	@echo ":show linker\n:show paths" | ghci -i"$(FOUND)" $<

%.build: %
	@

%.clean:
	@rm -f $(patsubst %.clean, %,    $@)
	@rm -f $(patsubst %.clean, %.o,  $@)
	@rm -f $(patsubst %.clean, %.hi, $@)

clean: $(patsubst %.hs, %.clean, $(FILES))
	@((find $(FIND) -name "*.o"     -exec rm -f {} \;); true)
	@((find $(FIND) -name "*.hi"    -exec rm -f {} \;); true)

echo:
	@echo $(FOUND)
	@#echo $(FILES) $(USED) $(FOUND)

